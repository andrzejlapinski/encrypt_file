package main

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"fmt"
	"io"
	"io/ioutil"
	mathRand "math/rand"
	"os"
	"time"
)

var src = mathRand.NewSource(time.Now().UnixNano())
var keyLen = 32
func main() {

	// if len(os.Args) != 2 {
	// 	fmt.Println("No files left")
	// 	os.Exit(1)
	// } else if !fileExist(os.Args[1]) {
	// 	fmt.Println("File does not exist!")
	// 	os.Exit(1)
	// }
	// fileName := os.Args[1]
	fileName := "test.txt"
	key := randomStringGenerator()
	fmt.Println("Key:", key)
	fileContent, err := loadFile(fileName)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	saveFile(encrypt(string(fileContent), key), fileName+".enc")
}

const letterBytes = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
const (
	letterIdxBits = 6
	letterIdxMask = 1<<letterIdxBits - 1
	letterIdxMax  = 63 / letterIdxBits
)

func loadFile(fileName string) ([]byte, error) {
	data, err := ioutil.ReadFile(fileName)
	return data, err
}

func saveFile(data string, fileName string) {
	ioutil.WriteFile(fileName, []byte(data), 777)
}

func fileExist(fileName string) bool {
	if s, err := os.Stat(fileName); s.IsDir() || err != nil {
		return false
	}
	return true
}

func randomStringGenerator() string {
	b := make([]byte, keyLen)
	for i, cache, remain := keyLen-1, src.Int63(), letterIdxMax; i >= 0; {
		if remain == 0 {
			cache, remain = src.Int63(), letterIdxMax
		}
		if idx := int(cache & letterIdxMask); idx < len(letterBytes) {
			b[i] = letterBytes[idx]
			i--
		}
		cache >>= letterIdxBits
		remain--
	}

	return string(b)
}

func encrypt(plainStr string, keyStr string) string {
	plainText := []byte(plainStr)
	key := []byte(keyStr)

	block, err := aes.NewCipher(key)
	if err != nil {
		panic(err)
	}
	cipherText := make([]byte, aes.BlockSize+len(plainText))

	iv := cipherText[:aes.BlockSize]

	if _, err := io.ReadFull(rand.Reader, iv); err != nil {
		panic(err)
	}

	stream := cipher.NewCFBEncrypter(block, iv)
	stream.XORKeyStream(cipherText[aes.BlockSize:], plainText)
	return string(cipherText)
}
