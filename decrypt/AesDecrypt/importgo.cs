using System;
using System.Runtime.InteropServices;

namespace HelloWorld
{
    public static class Hello 
    {
        [DllImport("export.dll", EntryPoint="PrintStuff")]
        static extern void PrintStuff();

        public static void DoGo()
        {
            PrintStuff();
        }

        static void Main() 
        {
            Console.WriteLine("Hello World!");
            PrintStuff();

            Console.ReadKey();
        }
    }
}